import Vue from 'vue'
import VueResource from 'vue-resource'
import App from './App.vue'

Vue.config.productionTip = false;

Vue.use(VueResource);
Vue.http.options.root = 'https://vue-ytb-form-2be3a.firebaseio.com';
// Vue.http.interceptors.push((request, next) => {
// 	console.log(request);
// 	if (request.method == 'POST') {
// 		request.method = 'PUT'
// 	}
// 	next(response => {
// 		response.json = () => {
// 			return {
// 				message: response.body
// 			}
// 		}
// 	});
// });

new Vue({
	render: h => h(App),
}).$mount('#app')
