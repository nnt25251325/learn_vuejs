import Vue from "vue";
import Router from "vue-router";
import Home from "./components/Home.vue";
import Header from "./components/layouts/Header.vue";
import Error404 from "./components/404.vue";
import User from "./components/user/User.vue";
import UserStart from "./components/user/Index.vue";
import UserDetails from "./components/user/UserDetails.vue";
import UserEdit from "./components/user/UserEdit.vue";

Vue.use(Router);

export default new Router({
	mode: 'history',
	routes: [
		{
			path: '/',
			name: 'homepage',
			component: Home,
			// components: {
			// 	default: Home,
			// 	header: Header
			// 	// 'header': Header
			// }
		},
		{
			path: '/user',
			name: 'user',
			component: User,
			children: [
				{
					path: '',
					name: 'index',
					component: UserStart
				},
				{
					path: ':id',
					name: 'userdetails',
					component: UserDetails,
					beforeEnter: (to, from, next) => {
						console.log('Action route guards');
						next();
					}
				},
				{
					path: ':id/edit',
					name: 'useredit',
					component: UserEdit
				}
			]
		},
		{
			path: '/auth-redirect',
			redirect: {
				name: 'homepage'
			}
		},
		{
			path: '/404',
			name: 'errorpage',
			component: Error404
		},
		{
			path: '*',
			redirect: '/404'
		}
	],
	scrollBehavior(to, from, savedPosition) {
		if (savedPosition) {
			return savedPosition;
		}
		if (to.hash) {
			return {
				selector: to.hash
			};
		}
	}
});
