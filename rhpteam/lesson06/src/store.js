import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		result: 0,
		value: ''
	},
	getters: {
		tenResult: state => {
			return state.result * 10;
		},
		nameResult: state => {
			return state.result + ' name product';
		},
		value: state => {
			return state.value;
		}
	},
	mutations: {
		increment(state) {
			state.result++;
		},
		// decrement2(state) {
		// 	state.result--;
		// },
		updateValue: (state, payload) => {
			state.value = payload;
		}
	},
	actions: {
		increment: ({ commit }) => {
			commit('increment');
		},
		updateValue: ({ commit }, payload) => {
			commit('updateValue', payload);
		}
	},
});
