var app = new Vue({
	el: '#app',
	data: {
		playerHealth: 100,
		monsterHealth: 100,
		gameIsRunning: false,
		turns: []
	},
	methods: {
		startNewGame: function() {
			this.gameIsRunning = true;
			this.playerHealth = 100;
			this.monsterHealth = 100;
			this.turns = [];
		},
		attack: function() {
			damage = this.inputDamage(4, 10);
			this.monsterHealth -= damage;
			this.turns.unshift({
				isPlayer: true,
				textLog: 'Player hits Monster for ' + damage
			});
			this.monsterAttack();
		},
		specialAttack: function() {
			damage = this.inputDamage(10, 20);
			this.monsterHealth -= damage;
			this.turns.unshift({
				isPlayer: true,
				textLog: 'Player hits Monster for ' + damage
			});
			this.monsterAttack();
		},
		heal: function() {
			//Player
			if (this.playerHealth <= 60) {
				this.playerHealth += 10;
			} else if (this.playerHealth > 60 && this.playerHealth < 70) {
				this.playerHealth = 70;
			} else {
				return false;
			}
			this.turns.unshift({
				isPlayer: true,
				textLog: 'Player heals for 10'
			});
			//Monster
			this.monsterAttack();
		},
		giveUp: function() {
			alert('You lost!');
			this.gameIsRunning = false;
			this.playerHealth = 0;
			this.monsterHealth = 100;
			this.turns = [];
		},
		monsterAttack: function() {
			damage = this.inputDamage(4, 10);
			this.playerHealth -= damage;
			this.turns.unshift({
				isPlayer: false,
				textLog: 'Monster hits Player for ' + damage
			});
			this.checkPlayerOptions();
		},
		inputDamage: function(minDamage, maxDamage) {
			return Math.max(Math.floor(Math.random() * maxDamage) + 1, minDamage);
		},
		checkPlayerOptions: function() {
			if (this.monsterHealth <= 0) {
				if (confirm('You won! New game?')) {
					this.startNewGame();
				} else {
					this.gameIsRunning = false;
					this.playerHealth = 0;
					this.monsterHealth = 0;
				}
			} else if (this.playerHealth <= 0) {
				if (confirm('You lost! New game?')) {
					this.startNewGame();
				} else {
					this.gameIsRunning = false;
					this.playerHealth = 0;
					this.monsterHealth = 0;
				}
			} else {
				return true;
			}
		}
	}
});
