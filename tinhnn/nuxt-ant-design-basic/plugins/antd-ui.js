// Sync with: assets/less/antd.less
// See: https://antdv.com/docs/vue/introduce/#Usage

import Vue from 'vue'
import {
  Modal,
  notification,
  Button,
  Checkbox,
  Dropdown,
  FormModel,
  Row,
  Col,
  Icon,
  Input,
  Layout,
  Menu,
  Select,
  Spin
} from 'ant-design-vue'

Vue.use(Modal)
Vue.use(notification)
Vue.use(Button)
Vue.use(Checkbox)
Vue.use(Dropdown)
Vue.use(FormModel)
Vue.use(Row)
Vue.use(Col)
Vue.use(Icon)
Vue.use(Input)
Vue.use(Layout)
Vue.use(Menu)
Vue.use(Select)
Vue.use(Spin)

Vue.prototype.$notification = notification
Vue.prototype.$confirm = Modal.confirm
