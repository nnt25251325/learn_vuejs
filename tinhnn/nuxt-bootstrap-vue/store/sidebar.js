/**
 * Copyright 2019 S-cubism inc. All rights reserved.
 */

import { SIDEBAR_TOGGLE } from '~/constants/mutation-types'

/**
 * State
 */
export const state = () => ({
  minimize: false
})

/**
 * Mutations
 */
export const mutations = {
  [SIDEBAR_TOGGLE]: state => {
    state.minimize = !state.minimize
  }
}

/**
 * Getters
 */
export const getters = {
  minimize: state => {
    return state.minimize
  }
}

/**
 * Actions
 */
export const actions = {
  toggle: ({ commit }) => {
    commit(SIDEBAR_TOGGLE)
  }
}

export default {
  state,
  mutations,
  getters,
  actions
}
