// Sync with: assets/less/antd.less
// See: https://antdv.com/docs/vue/introduce/#Usage

import Vue from 'vue'
import { BootstrapVue } from 'bootstrap-vue'

Vue.use(BootstrapVue)
