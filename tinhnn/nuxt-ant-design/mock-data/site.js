/**
 * Copyright 2020 Digi Dinos JSC. All rights reserved.
 * Email: tech@digidinos.com.
 */

export default {
  getSiteList: () => {
    return {
      data: [
        {
          id: 17,
          name: '時事ドットコム',
          file_id: 17,
          file: {
            created_at: 1591064267,
            id: 17,
            path: 'https://loremflickr.com/320/240',
            updated_at: 1591064267
          },
          status: 1,
          description: '',
          updated_at: 1591064201,
          url: 'https://www.jiji.com'
        },
        {
          id: 16,
          name: 'ganas 開発メディア',
          file_id: 16,
          file: {
            created_at: 1591064267,
            id: 16,
            path: 'https://loremflickr.com/320/240',
            updated_at: 1591064267
          },
          status: 1,
          description: '',
          updated_at: 1591064201,
          url: 'https://www.ganas.or.jp'
        },
        {
          id: 15,
          name: 'アフリカ専門ニュースメディア',
          file_id: 15,
          file: {
            created_at: 1591064267,
            id: 15,
            path: 'https://loremflickr.com/320/240',
            updated_at: 1591064267
          },
          status: 1,
          description: '',
          updated_at: 1591064201,
          url: 'https://afri-quest.com'
        },
        {
          id: 14,
          name: 'ALL ABOUT AFRICA-アフリカ総合ウェブメディア-',
          file_id: 14,
          file: {
            created_at: 1591064267,
            id: 14,
            path: 'https://loremflickr.com/320/240',
            updated_at: 1591064267
          },
          status: 0,
          description: '',
          updated_at: 1591064201,
          url: 'http://all-about-africa.com'
        },
        {
          id: 13,
          name: 'News & Information',
          file_id: 13,
          file: {
            created_at: 1591064267,
            id: 13,
            path: 'https://loremflickr.com/320/240',
            updated_at: 1591064267
          },
          status: 1,
          description: '',
          updated_at: 1591064201,
          url: 'https://www.usaid.gov/news-information'
        },
        {
          id: 12,
          name: 'NEWS',
          file_id: 12,
          file: {
            created_at: 1591064267,
            id: 12,
            path: 'https://loremflickr.com/320/240',
            updated_at: 1591064267
          },
          status: 0,
          description: '',
          updated_at: 1591064201,
          url: 'https://ec.europa.eu/international-partnerships/newsroom/news'
        },
        {
          id: 11,
          name: 'UN NEWS',
          file_id: 11,
          file: {
            created_at: 1591064267,
            id: 11,
            path: 'https://loremflickr.com/320/240',
            updated_at: 1591064267
          },
          status: 0,
          description: '',
          updated_at: 1591064201,
          url: 'https://news.un.org/en/'
        },
        {
          id: 10,
          name: 'ニュースリリース ',
          file_id: 10,
          file: {
            created_at: 1591064267,
            id: 10,
            path: 'https://loremflickr.com/320/240',
            updated_at: 1591064267
          },
          status: 1,
          description: '',
          updated_at: 1591064201,
          url: 'https://www.adb.org/ja/japan/news'
        },
        {
          id: 9,
          name: 'JICA - 国際協力機構',
          file_id: 9,
          file: {
            created_at: 1591064267,
            id: 9,
            path: 'https://loremflickr.com/320/240',
            updated_at: 1591064267
          },
          status: 1,
          description: '',
          updated_at: 1591064201,
          url: 'https://www.jica.go.jp'
        },
        {
          id: 8,
          name: 'ジェトロ（日本貿易振興機構）',
          file_id: 8,
          file: {
            created_at: 1591064267,
            id: 8,
            path: 'https://loremflickr.com/320/240',
            updated_at: 1591064267
          },
          status: 1,
          description: '',
          updated_at: 1591064201,
          url: 'https://www.jetro.go.jp'
        },
        {
          id: 7,
          name: 'JBIC 国際協力銀行 ',
          file_id: 7,
          file: {
            created_at: 1591064267,
            id: 7,
            path: 'https://loremflickr.com/320/240',
            updated_at: 1591064267
          },
          status: 1,
          description: '',
          updated_at: 1591064201,
          url: 'https://www.jbic.go.jp/ja/index.html'
        },
        {
          id: 6,
          name: '外務省ホームページ（日本語）',
          file_id: 6,
          file: {
            created_at: 1591064267,
            id: 6,
            path: 'https://loremflickr.com/320/240',
            updated_at: 1591064267
          },
          status: 1,
          description: '',
          updated_at: 1591064201,
          url: 'https://www.mofa.go.jp/mofaj/index.html'
        },
        {
          id: 5,
          name: '国際協力NGOジャパン',
          file_id: 5,
          file: {
            created_at: 1591064267,
            id: 5,
            path: 'https://loremflickr.com/320/240',
            updated_at: 1591064267
          },
          status: 1,
          description: '',
          updated_at: 1591064201,
          url: 'https://www.japanplatform.org'
        },
        {
          id: 4,
          name: '国際ニュース',
          file_id: 4,
          file: {
            created_at: 1591064267,
            id: 4,
            path: 'https://loremflickr.com/320/240',
            updated_at: 1591064267
          },
          status: 1,
          description: '',
          updated_at: 1591064201,
          url: 'https://www.afpbb.com'
        },
        {
          id: 3,
          name: 'ロイター',
          file_id: 3,
          file: {
            created_at: 1591064267,
            id: 3,
            path: 'https://loremflickr.com/320/240',
            updated_at: 1591064267
          },
          status: 1,
          description: '',
          updated_at: 1591064201,
          url: 'https://jp.reuters.com'
        },
        {
          id: 2,
          name: 'CNN NEWS',
          file_id: 2,
          file: {
            created_at: 1591064267,
            id: 2,
            path: 'https://loremflickr.com/320/240',
            updated_at: 1591064267
          },
          status: 1,
          description: '',
          updated_at: 1591064201,
          url: 'https://www.cnn.co.jp'
        },
        {
          id: 1,
          name: 'BBC NEWS',
          file_id: 1,
          file: {
            created_at: 1591064267,
            id: 1,
            path: 'https://loremflickr.com/320/240',
            updated_at: 1591064267
          },
          status: 1,
          description: '',
          updated_at: 1591064201,
          url: 'https://www.bbc.com/japanese'
        }
      ],
      links: {
        first: 'http://content-management-api.test/api/admin/site?page=1',
        last: 'http://content-management-api.test/api/admin/site?page=1',
        prev: null,
        next: null
      },
      meta: {
        current_page: 1,
        from: 1,
        last_page: 1,
        path: 'http://content-management-api.test/api/admin/site',
        per_page: 20,
        to: 17,
        total: 17
      }
    }
  },

  createSite: () => {
    return {
      data: {
        id: 17,
        name: '時事ドットコム',
        file_id: 17,
        file: {
          created_at: 1591064267,
          id: 17,
          path: 'https://loremflickr.com/320/240',
          updated_at: 1591064267
        },
        status: 1,
        description: '',
        updated_at: 1591064201,
        url: 'https://www.jiji.com'
      }
    }
  },

  getSite: () => {
    return {
      data: {
        id: 17,
        name: '時事ドットコム',
        file_id: 17,
        file: {
          created_at: 1591064267,
          id: 17,
          path: 'https://loremflickr.com/320/240',
          updated_at: 1591064267
        },
        status: 1,
        description: '',
        updated_at: 1591064201,
        url: 'https://www.jiji.com'
      }
    }
  },

  updateSite: () => {
    return {
      data: {
        id: 17,
        name: '時事ドットコム',
        file_id: 17,
        file: {
          created_at: 1591064267,
          id: 17,
          path: 'https://loremflickr.com/320/240',
          updated_at: 1591064267
        },
        status: 1,
        description: '',
        updated_at: 1591064201,
        url: 'https://www.jiji.com'
      }
    }
  },

  deleteSite: () => {
    return {}
  }
}
