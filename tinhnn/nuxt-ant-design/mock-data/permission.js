/**
 * Copyright 2020 Digi Dinos JSC. All rights reserved.
 * Email: tech@digidinos.com.
 */

export default {
  getPermission: () => {
    return {
      data: [
        {
          id: 1,
          name: 'role.index'
        },
        {
          id: 2,
          name: 'role.store'
        },
        {
          id: 3,
          name: 'role.show'
        },
        {
          id: 4,
          name: 'role.update'
        },
        {
          id: 5,
          name: 'role.delete'
        },
        {
          id: 6,
          name: 'user.index'
        },
        {
          id: 7,
          name: 'user.store'
        },
        {
          id: 8,
          name: 'user.show'
        },
        {
          id: 9,
          name: 'user.update'
        },
        {
          id: 10,
          name: 'user.delete'
        },
        {
          id: 11,
          name: 'menu.index'
        },
        {
          id: 12,
          name: 'menu.store'
        },
        {
          id: 13,
          name: 'menu.show'
        },
        {
          id: 14,
          name: 'menu.update'
        },
        {
          id: 15,
          name: 'menu.delete'
        }
      ]
    }
  }
}
