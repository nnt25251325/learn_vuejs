/**
 * Copyright 2020 Digi Dinos JSC. All rights reserved.
 * Email: tech@digidinos.com.
 */

export default {
  getDashboardInfo: () => {
    return {
      total_site: 17,
      total_category: 15,
      total_article: 1941,
      total_view: '973288',
      last_customer_info: [
        {
          age: 93,
          created_at: 1591064277,
          deleted_at: null,
          gender: 1,
          id: 100,
          name: 'Dr. Gerson Weimann Jr.',
          platform: 1,
          updated_at: 1591064277,
          user_agent: 'Esse in aut et. Architecto soluta molestias ratione et quaerat eum.'
        },
        {
          age: 34,
          created_at: 1591064277,
          deleted_at: null,
          gender: 1,
          id: 99,
          name: 'Ola Cruickshank PhD',
          platform: 1,
          updated_at: 1591064277,
          user_agent: 'Cupiditate illum ut quisquam sequi quos. Corrupti error voluptatem quod rem. Quos consequatur placeat nisi non.'
        },
        {
          age: 58,
          created_at: 1591064277,
          deleted_at: null,
          gender: 1,
          id: 98,
          name: 'Carissa Cremin MD',
          platform: 0,
          updated_at: 1591064277,
          user_agent: 'Qui dolorem et repellat in. Commodi delectus enim dolorum repudiandae sapiente distinctio. Laboriosam non rerum sequi ipsa.'
        },
        {
          age: 37,
          created_at: 1591064277,
          deleted_at: null,
          gender: 0,
          id: 97,
          name: 'Tre King',
          platform: 0,
          updated_at: 1591064277,
          user_agent: 'Est officia provident in cumque. Ab eum voluptatem officia tempora. Inventore consequuntur quis non facere nemo praesentium. Unde fugit ipsum eum deleniti doloribus consequatur magnam.'
        },
        {
          age: 82,
          created_at: 1591064277,
          deleted_at: null,
          gender: 0,
          id: 96,
          name: 'Elizabeth Runte DVM',
          platform: 0,
          updated_at: 1591064277,
          user_agent: 'Sint dolor odio est asperiores ea nihil odio. Deserunt non ullam velit id blanditiis minus totam. Optio ipsam consequuntur quis et nihil incidunt porro. Facilis nihil commodi consequatur rem facere minima qui.'
        },
        {
          age: 55,
          created_at: 1591064277,
          deleted_at: null,
          gender: 1,
          id: 95,
          name: 'Rubye Cassin V',
          platform: 1,
          updated_at: 1591064277,
          user_agent: 'Et nobis sed rerum nostrum tenetur perferendis modi aut. Quae perspiciatis possimus fugit.'
        },
        {
          age: 71,
          created_at: 1591064277,
          deleted_at: null,
          gender: 1,
          id: 94,
          name: 'Kamron Abshire',
          platform: 2,
          updated_at: 1591064277,
          user_agent: 'Aut voluptate quis ut amet harum ut vel. Quos et ullam velit adipisci ut. Possimus quibusdam iste velit enim dolorum inventore temporibus. Omnis sed distinctio ea corrupti.'
        },
        {
          age: 61,
          created_at: 1591064276,
          deleted_at: null,
          gender: 0,
          id: 93,
          name: 'Mr. Donnie Nienow Jr.',
          platform: 1,
          updated_at: 1591064276,
          user_agent: 'Sed cum cum est maxime incidunt impedit minima. Voluptatem sint impedit aut molestiae quasi. Vel ea eos rerum officiis.'
        },
        {
          age: 24,
          created_at: 1591064276,
          deleted_at: null,
          gender: 1,
          id: 92,
          name: 'Vincenza Klein',
          platform: 2,
          updated_at: 1591064276,
          user_agent: 'Asperiores consequatur earum voluptatum minima. Iusto dolorem dolores voluptas pariatur molestias illum. Enim eos natus laborum suscipit aut magnam. Facere quia nisi porro.'
        },
        {
          age: 50,
          created_at: 1591064276,
          deleted_at: null,
          gender: 0,
          id: 91,
          name: 'Charlie Pfannerstill',
          platform: 0,
          updated_at: 1591064276,
          user_agent: 'Eligendi aspernatur sit est consequatur omnis. Debitis voluptatem libero ut quos. Ullam iure ex soluta dolores eaque officia placeat atque.'
        }
      ]
    }
  }
}
