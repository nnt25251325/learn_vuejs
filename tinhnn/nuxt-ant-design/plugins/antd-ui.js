/*
 * Copyright 2020 Digi Dinos JSC. All rights reserved.
 * Email: tech@digidinos.com.
 */

import Vue from 'vue'
import Antd from 'ant-design-vue/lib'

Vue.use(Antd)
