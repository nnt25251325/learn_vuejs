/*
 * Copyright 2020 Digi Dinos JSC. All rights reserved.
 * Email: tech@digidinos.com.
 */

'use strict'

const CONFIG = {
  DEFAULT_API_GET_RETRY_COUNT: 3,
  DEFAULT_REFRESH_TOKEN_RETRY_COUNT: 3,

  LS_KEY_PREFIX: 'dam',
  LS_KEY_SESSION_TOKEN: 'dd-trec-dam',
  AUTH_ID: 'id'
}
export default CONFIG
