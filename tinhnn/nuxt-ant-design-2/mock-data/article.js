/**
 * Copyright 2020 Digi Dinos JSC. All rights reserved.
 * Email: tech@digidinos.com.
 */

export default {
  getArticleList: () => {
    return {
      data: [
        {
          admin_note: null,
          article_category: {
            created_at: 1591064201,
            description: '',
            id: 6,
            name: '環境',
            parent: null,
            parent_id: 0,
            position: 5,
            updated_at: 1591064201
          },
          article_category_id: 6,
          create_origin_at: null,
          created_at: 1589538089,
          description: null,
          hash_md5: null,
          id: 1941,
          image_path: 'https://s4.reutersmedia.net/resources/r/?m=02&d=20200512&t=2&i=1518409665&w=1200&r=LYNXMPEG4B24X',
          images: [
            {
              created_at: 1591064267,
              id: 19,
              path: '/upload/demo2.png',
              pivot: {
                article_id: 1941,
                file_id: 19,
                created_at: null,
                updated_at: null
              },
              updated_at: 1591064267
            }
          ],
          last_crawl_at: null,
          origin_url: 'https://jp.reuters.com/article/f-idJPKBN22O3GG',
          origin_url_md5: null,
          public_end_at: 1689338418,
          public_start_at: 1589512889,
          related_article: null,
          short_description: '自動車レースＦ１、フェラーリは１２日、個人総合優勝４回のセバスチャン・フェテル（３２、ドイツ）との契約を更新せず、今年限りでチームを離れることを発表した。',
          site: {
            id: 3,
            name: 'ロイター',
            file_id: 3,
            status: 1,
            description: '',
            updated_at: 1591064201,
            url: 'https://jp.reuters.com'
          },
          site_id: 3,
          status: 3,
          title: 'Ｆ１＝フェテル、20年限りでフェラーリを離れることに - ロイター',
          type: 0,
          update_origin_at: null,
          updated_at: 1591169939,
          views: 179,
          youtube_id: '',
          youtube_url: null,
          youtube_views: 691
        },
        {
          admin_note: null,
          article_category: {
            created_at: 1591064201,
            description: '',
            id: 10,
            name: '教育',
            parent: null,
            parent_id: 0,
            position: 9,
            updated_at: 1591064201
          },
          article_category_id: 10,
          create_origin_at: null,
          created_at: 1589538089,
          description: null,
          hash_md5: null,
          id: 1940,
          image_path: 'https://s4.reutersmedia.net/resources/r/?m=02&d=20200512&t=2&i=1518409666&w=1200&r=LYNXMPEG4B24Z',
          images: [
            {
              created_at: 1591064267,
              id: 20,
              path: '/upload/demo3.jpg',
              pivot: {
                article_id: 1940,
                file_id: 20,
                created_at: null,
                updated_at: null
              },
              updated_at: 1591064267
            }
          ],
          last_crawl_at: null,
          origin_url: 'https://jp.reuters.com/article/health-coronavirus-usa-la-idJPKBN22O3GB',
          origin_url_md5: null,
          public_end_at: 1689338418,
          public_start_at: 1589512889,
          related_article: null,
          short_description: '米カリフォルニア州ロサンゼルス（ＬＡ）のガルセッティ市長は１２日、保健当局者が外出制限を少なくとも７月まで延長すると発言したことが市民の反発を呼んでいることについて、制限が全く緩和されないという意味ではないと釈明した。',
          site: {
            id: 3,
            name: 'ロイター',
            file_id: 3,
            status: 1,
            description: '',
            updated_at: 1591064201,
            url: 'https://jp.reuters.com'
          },
          site_id: 3,
          status: 1,
          title: '米ＬＡ市長、外出制限延長巡る保健当局の発言を修正　批判受け - ロイター',
          type: 0,
          update_origin_at: null,
          updated_at: 1591169939,
          views: 773,
          youtube_id: '',
          youtube_url: null,
          youtube_views: 944
        },
        {
          admin_note: null,
          article_category: null,
          article_category_id: 0,
          create_origin_at: null,
          created_at: 1589538089,
          description: null,
          hash_md5: null,
          id: 1939,
          image_path: 'https://s4.reutersmedia.net/resources_v2/images/rcom-default.png',
          images: [],
          last_crawl_at: null,
          origin_url: 'https://jp.reuters.com/article/idJP00049500_20200513_00220200512',
          origin_url_md5: null,
          public_end_at: 1689338418,
          public_start_at: 1589512889,
          related_article: null,
          short_description: '*08:49JST <6232>  ACSL  -  - 3月17日安値1612円をボトムにリバウンドをみせており、上昇する25日線を支持線に、上値抵抗として意識されていた75日線を突破してきている。一目均衡表では上昇する転換線を支持線に、雲上限を捉えてきている。遅行スパンは実線を上放れて推移しているため、上方シグナルが継続。 《FA》',
          site: {
            id: 3,
            name: 'ロイター',
            file_id: 3,
            status: 1,
            description: '',
            updated_at: 1591064201,
            url: 'https://jp.reuters.com'
          },
          site_id: 3,
          status: 2,
          title: 'ACSL---上値抵抗として意識されていた75日線を突破 - ロイター',
          type: 0,
          update_origin_at: null,
          updated_at: 1591169939,
          views: 914,
          youtube_id: '',
          youtube_url: null,
          youtube_views: 476
        },
        {
          admin_note: null,
          article_category: {
            created_at: 1591064201,
            description: '',
            id: 1,
            name: '平和・安全',
            parent: null,
            parent_id: 0,
            position: 0,
            updated_at: 1591064201
          },
          article_category_id: 1,
          create_origin_at: null,
          created_at: 1589538089,
          description: null,
          hash_md5: null,
          id: 1938,
          image_path: 'https://s4.reutersmedia.net/resources_v2/images/rcom-default.png',
          images: [],
          last_crawl_at: null,
          origin_url: 'https://jp.reuters.com/article/tokyo-dbt-idJPL4N2CU4N2',
          origin_url_md5: null,
          public_end_at: 1689338418,
          public_start_at: 1589512889,
          related_article: null,
          short_description: '＜０８：４７＞　国債先物は続伸で寄り付く、需給懸念がいったん後退\n    \n    国債先物中心限月６月限は前営業日比４銭高の１５２円３５銭と続伸で寄り付いた。\n新型コロナウイルス感染再拡大への懸念に加え、日米で１０年債入札が好調な結果となり\n、需給懸念がいったん後退している。\n    一方、市場では「中央銀行が国債の大部分を吸収する需給構造は健全とは言えない。\n潜在的な金利上昇懸念は残る」（国内銀行）との声も出ている。\n\n    \n TRADEWEB                                \n           OFFER   BID     前日比  時間\n 2年       -0.174  -0.164       0    8:45\n 5年       -0.128  -0.118       0    8:45\n 10年      -0.017  -0.007  -0.004    8:46\n 20年       0.316   0.325  -0.004    8:45\n 30年       0.447   0.459       0    8:45\n 40年       0.456   0.468  -0.001    8:45\n \n\n    \n\n\n    国債引値　メニュー\n    １０年物国債先物\n    国債引値一覧(１０年債)・入札前取引含む　\n    国債引値一覧(２０年債)・入札前取引含む　\n    国債引値一覧(３０年債)・入札前取引含む　\n    国債引値一覧(２・４・５・６年債)・入札前取引含　\n    変動利付国債引値一覧・入札前取引含む　\n    物価連動国債引値一覧・入札前取引含む　\n    スワップ金利動向         \n    ユーロ円金利先物（ＴＦＸ）\n    ユーロ円金利先物（ＳＧＸ）\n    無担保コールオーバーナイト金利先物（ＴＦＸ）\n    ＴＩＢＯＲレート\n    日本証券業協会　売買参考統計値（１０年債）\n    日本証券業協会　売買参考統計値（２０年債）\n    日本証券業協会　売買参考統計値（３０年債）\n    日本証券業協会　売買参考統計値（４０年債）\n    日本証券業協会　短期国債レート・入札前取引含む\n    短期国債引け値・入札前取引含む　\n    短期金利のインデックス　\n\n    \n (※関連情報は画面右側にある「関連コンテンツ」メニューからご覧ください)',
          site: {
            id: 3,
            name: 'ロイター',
            file_id: 3,
            status: 1,
            description: '',
            updated_at: 1591064201,
            url: 'https://jp.reuters.com'
          },
          site_id: 3,
          status: 3,
          title: '〔マーケットアイ〕金利：国債先物は続伸で寄り付く、需給懸念がいったん後退 - ロイター',
          type: 0,
          update_origin_at: null,
          updated_at: 1591169939,
          views: 457,
          youtube_id: '',
          youtube_url: null,
          youtube_views: 80
        },
        {
          admin_note: null,
          article_category: {
            created_at: 1591064201,
            description: '',
            id: 12,
            name: 'ガバナンス',
            parent: null,
            parent_id: 0,
            position: 11,
            updated_at: 1591064201
          },
          article_category_id: 12,
          create_origin_at: null,
          created_at: 1589538089,
          description: null,
          hash_md5: null,
          id: 1937,
          image_path: 'https://s4.reutersmedia.net/resources_v2/images/rcom-default.png',
          images: [
            {
              created_at: 1591064267,
              id: 19,
              path: '/upload/demo2.png',
              pivot: {
                article_id: 1937,
                file_id: 19,
                created_at: null,
                updated_at: null
              },
              updated_at: 1591064267
            }
          ],
          last_crawl_at: null,
          origin_url: 'https://jp.reuters.com/article/tokyo-stx-idJPL4N2CU4UR',
          origin_url_md5: null,
          public_end_at: 1689338418,
          public_start_at: 1589512889,
          related_article: null,
          short_description: '＜０８：４５＞　寄り前の板状況、輸出関連株や景気敏感株に売り優勢目立つ\n    \n    市場関係者によると、寄り前の板状況は、トヨタ自動車        、ホンダ        、\nキヤノン        、ソニー        など主力の輸出関連株が売り優勢。指数寄与度の大き\nいファーストリテイリング        、ファナック        のほか、日本製鉄        など\n景気敏感株の一角も売り優勢が目立っている。\n    \n    \n    東証第１部出来高上位５０銘柄       \n    東証第１部値上がり率上位５０銘柄       \n    東証第１部値下がり率上位５０銘柄       \n    日経２２５先物        \n    ＳＧＸ日経２２５先物        \n    ＴＯＰＩＸ先物        \n    日経２２５オプション            \n    株式関連指標の索引ページ        \n    関連アプリ：インデックスムーバー（リフィニティブEIKON検索ボックスで“IMO”と\n入力）\n\n    \n (',
          site: {
            id: 3,
            name: 'ロイター',
            file_id: 3,
            status: 1,
            description: '',
            updated_at: 1591064201,
            url: 'https://jp.reuters.com'
          },
          site_id: 3,
          status: 3,
          title: '〔マーケットアイ〕株式：寄り前の板状況、輸出関連株や景気敏感株に売り優勢目立つ - ロイター',
          type: 0,
          update_origin_at: null,
          updated_at: 1591169939,
          views: 247,
          youtube_id: '',
          youtube_url: null,
          youtube_views: 962
        },
        {
          admin_note: null,
          article_category: {
            created_at: 1591064201,
            description: '',
            id: 3,
            name: '農業・漁業',
            parent: null,
            parent_id: 0,
            position: 2,
            updated_at: 1591064201
          },
          article_category_id: 3,
          create_origin_at: null,
          created_at: 1589538089,
          description: null,
          hash_md5: null,
          id: 1936,
          image_path: 'https://s4.reutersmedia.net/resources_v2/images/rcom-default.png',
          images: [
            {
              created_at: 1591064267,
              id: 18,
              path: '/upload/demo1.jpeg',
              pivot: {
                article_id: 1936,
                file_id: 18,
                created_at: null,
                updated_at: null
              },
              updated_at: 1591064267
            }
          ],
          last_crawl_at: null,
          origin_url: 'https://jp.reuters.com/article/idJP00093400_20200513_01820200512',
          origin_url_md5: null,
          public_end_at: 1689338418,
          public_start_at: 1589512889,
          related_article: null,
          short_description: '*08:43JST 前場に注目すべき3つのポイント～2万円に接近する局面においては、押し目拾いのスタンスに 13日前場の取引では以下の３つのポイントに注目したい。 ■株式見通し：2万円に接近する局面においては、押し目拾いのスタンスに ■前場の注目材料：ホンダ、20/3営業利益12.8％減 6336億円 ■SCREEN、半導体製造装置の受注堅調 ■2万円に接近する局面においては、押し目拾いのスタンスに 13日の日本株市場は、やや神経質な展開になりそうだ。12日の米国市場では、NYダウが457ドル安だった。米国政府の新型ウイルス対策チームを率いる国立アレルギー・感染症研究所のファウチ所長が上院での証言で、政府が示したルールに従わない早過ぎる経済活動の再開に警鐘を鳴らしたためウイルス感染「第2波」への警戒感が高まった。シカゴ日経225先物清算値は大阪比145円安の20145円。円相場は1ドル107円10銭台で推移している。 シカゴ先物にサヤ寄せする格好から売り先行の展開になりやすく、日経平均は2万円の攻防が意識されやすいだろう。ただし、昨日段階で買い戻しの流れも一巡感があっただけに、嫌気売りが強まる流れにはならないだろう。反対に2万円に接近する局面においては、押し目拾いのスタンスに向かわせやすいとみておきたい。 また、決算発表がピークを迎えているため、機関投資家の売買も細っているため、薄商いの中をインデックス売買に振らされやすい展開も想定されるが、短期的な売買が中心と考えられるため、底堅さが意識される局面においては、反対売買も速いと考えられる。また、2万円に迫る局面においては、日銀のETF買い入れへの思惑も高まりやすく、需給面での下支えになりそうだ。 もっとも、決算では主要な企業の冴えない決算が重石になりやすい。まずは、昨日決算を発表したトヨタ<7203>辺りの底堅さを見極めたいところである。また、米国市場では、感染第2波への警戒感が高まっているため、新型コロナの治療薬や診断薬などを手掛けている銘柄への物色も意識されやすい。その他、個人主体の売買では、足元で急ピッチの調整が続いているバイオ株なども、スピード調整からの自律反発狙いの動きが意識されそうである。 ■ホンダ、20/3営業利益12.8％減 6336億円 ホンダ<7267>が発表した2020年3月期決算は、売上高に当たる売上収益は6％減の14兆9310億円、営業利益は同12.8％減の6336億円だった。新型コロナウイルスの感染拡大による影響で世界的に自動車などの生産や販売が振るわなかった。四輪事業はアジアや北米を中心に販売が低迷。二輪事業はインドなどアジア地域の販売が減少した。21年3月期の業績予想と配当予想は「未定」とした。 ■前場の注目材料 ・米原油先物は上昇（25.78、+1.64） ・米長期金利は低下 ・日銀のETF購入 ・新型コロナウイルス治療薬開発 ・海外の経済活動再開 ・日米欧の大型財政出動 ・株安局面での自社株買い ・7-9月期の業績回復期待 ・トヨタ自<7203>今期見通し、営業益8割減、コロナ禍で世界販売急落 ・シスメックス<6869>検査法開発に着手、初期段階で重症化予測 ・長瀬産業<8012>医療VBと資本提携、画像処理技術を活用 ・SCREEN<7735>半導体製造装置の受注堅調 ・三菱UFJ<8306>社債600億円、中小の資金繰り支援 ・ショーワ<7274>全社にRPA導入、間接業務を月1000時間削減 ・小松マテーレ<3580>衣料向け新素材開発、植物由来で吸水拡散性 ☆前場のイベントスケジュール ＜国内＞ ・08:50　3月経常収支（予想：+2兆342億円、2月：+3兆1688億円） ＜海外＞ ・11:00　NZ準備銀行が政策金利発表（0.25％に据え置き予想） 《SF》',
          site: {
            id: 3,
            name: 'ロイター',
            file_id: 3,
            status: 1,
            description: '',
            updated_at: 1591064201,
            url: 'https://jp.reuters.com'
          },
          site_id: 3,
          status: 3,
          title: '前場に注目すべき3つのポイント～2万円に接近する局面においては、押し目拾いのスタンスに - ロイター',
          type: 0,
          update_origin_at: null,
          updated_at: 1591169939,
          views: 382,
          youtube_id: '',
          youtube_url: null,
          youtube_views: 181
        },
        {
          admin_note: null,
          article_category: {
            created_at: 1591064201,
            description: '',
            id: 6,
            name: '環境',
            parent: null,
            parent_id: 0,
            position: 5,
            updated_at: 1591064201
          },
          article_category_id: 6,
          create_origin_at: null,
          created_at: 1589538088,
          description: null,
          hash_md5: null,
          id: 1935,
          image_path: 'https://s4.reutersmedia.net/resources_v2/images/rcom-default.png',
          images: [
            {
              created_at: 1591064267,
              id: 20,
              path: '/upload/demo3.jpg',
              pivot: {
                article_id: 1935,
                file_id: 20,
                created_at: null,
                updated_at: null
              },
              updated_at: 1591064267
            }
          ],
          last_crawl_at: null,
          origin_url: 'https://jp.reuters.com/article/idJP00049500_20200513_00120200512',
          origin_url_md5: null,
          public_end_at: 1689338418,
          public_start_at: 1589512888,
          related_article: null,
          short_description: '*08:42JST <3814>  アルファクス  -  - 4月6日安値302円をボトムにリバウンドをみせており、上昇する25日線を支持線に、上値抵抗の75日線に接近。一目均衡表では転換線を支持線に、雲下限を突破してきている。厚い雲のため強弱感が対立しやすいが、遅行スパンは実線を上放れて推移しているため、上方シグナルが継続。 《FA》',
          site: {
            id: 3,
            name: 'ロイター',
            file_id: 3,
            status: 1,
            description: '',
            updated_at: 1591064201,
            url: 'https://jp.reuters.com'
          },
          site_id: 3,
          status: 3,
          title: 'アルファクス---上値抵抗の75日線に接近 - ロイター',
          type: 0,
          update_origin_at: null,
          updated_at: 1591169939,
          views: 827,
          youtube_id: '',
          youtube_url: null,
          youtube_views: 945
        },
        {
          admin_note: null,
          article_category: {
            created_at: 1591064201,
            description: '',
            id: 9,
            name: 'IT',
            parent: null,
            parent_id: 0,
            position: 8,
            updated_at: 1591064201
          },
          article_category_id: 9,
          create_origin_at: null,
          created_at: 1589538088,
          description: null,
          hash_md5: null,
          id: 1934,
          image_path: 'https://s4.reutersmedia.net/resources_v2/images/rcom-default.png',
          images: [],
          last_crawl_at: null,
          origin_url: 'https://jp.reuters.com/article/idJP00054400_20200513_00120200512',
          origin_url_md5: null,
          public_end_at: 1689338418,
          public_start_at: 1589512888,
          related_article: null,
          short_description: '*08:40JST 2万円に接近する局面においては、押し目拾いのスタンスに 　13日の日本株市場は、やや神経質な展開になりそうだ。12日の米国市場では、NYダウが457ドル安だった。米国政府の新型ウイルス対策チームを率いる国立アレルギー・感染症研究所のファウチ所長が上院での証言で、政府が示したルールに従わない早過ぎる経済活動の再開に警鐘を鳴らしたためウイルス感染「第2波」への警戒感が高まった。シカゴ日経225先物清算値は大阪比145円安の20145円。円相場は1ドル107円10銭台で推移している。 　シカゴ先物にサヤ寄せする格好から売り先行の展開になりやすく、日経平均は2万円の攻防が意識されやすいだろう。ただし、昨日段階で買い戻しの流れも一巡感があっただけに、嫌気売りが強まる流れにはならないだろう。反対に2万円に接近する局面においては、押し目拾いのスタンスに向かわせやすいとみておきたい。 　また、決算発表がピークを迎えているため、機関投資家の売買も細っているため、薄商いの中をインデックス売買に振らされやすい展開も想定されるが、短期的な売買が中心と考えられるため、底堅さが意識される局面においては、反対売買も速いと考えられる。また、2万円に迫る局面においては、日銀のETF買い入れへの思惑も高まりやすく、需給面での下支えになりそうだ。 　もっとも、決算では主要な企業の冴えない決算が重石になりやすい。まずは、昨日決算を発表したトヨタ<7203>辺りの底堅さを見極めたいところである。また、米国市場では、感染第2波への警戒感が高まっているため、新型コロナの治療薬や診断薬などを手掛けている銘柄への物色も意識されやすい。その他、個人主体の売買では、足元で急ピッチの調整が続いているバイオ株なども、スピード調整からの自律反発狙いの動きが意識されそうである。 《AK》',
          site: {
            id: 3,
            name: 'ロイター',
            file_id: 3,
            status: 1,
            description: '',
            updated_at: 1591064201,
            url: 'https://jp.reuters.com'
          },
          site_id: 3,
          status: 3,
          title: '2万円に接近する局面においては、押し目拾いのスタンスに - ロイター',
          type: 0,
          update_origin_at: null,
          updated_at: 1591169939,
          views: 845,
          youtube_id: '',
          youtube_url: null,
          youtube_views: 386
        },
        {
          admin_note: null,
          article_category: {
            created_at: 1591064201,
            description: '',
            id: 9,
            name: 'IT',
            parent: null,
            parent_id: 0,
            position: 8,
            updated_at: 1591064201
          },
          article_category_id: 9,
          create_origin_at: null,
          created_at: 1589538088,
          description: null,
          hash_md5: null,
          id: 1933,
          image_path: 'https://s4.reutersmedia.net/resources_v2/images/rcom-default.png',
          images: [
            {
              created_at: 1591064267,
              id: 20,
              path: '/upload/demo3.jpg',
              pivot: {
                article_id: 1933,
                file_id: 20,
                created_at: null,
                updated_at: null
              },
              updated_at: 1591064267
            }
          ],
          last_crawl_at: null,
          origin_url: 'https://jp.reuters.com/article/idJP00093400_20200513_01620200512',
          origin_url_md5: null,
          public_end_at: 1689338418,
          public_start_at: 1589512888,
          related_article: null,
          short_description: '*08:35JST 今日の為替市場ポイント：米国株安などを意識してリスク選好のドル買い抑制も 12日のドル・円は、東京市場では107円70銭から107円35銭まで下落。欧米市場でドルは107円56銭から107円12銭まで続落し、107円15銭で取引終了。 本日13日のドル・円は、主に107円台前半で推移か。米長期金利の低下や米国株安を意識して、リスク選好的なドル買い・円売りは抑制される可能性がある。 市場関係者の間では、19日に行なわれる米連邦準備制度理事会（FRB）のパウエル議長とムニューシン米財務長官の議会証言（上院銀行委員会の公聴会）に対する関心が高まっている。一部の市場関係者は「主要経済指標の内容に注意を払う必要があるのは当然だが、経済見通しについてのFRB議長の最新の見解が確認できるため、議会証言の発言内容は大変重要な手掛かり材料になる」と指摘している。 債券市場では、米国経済のすみやかな回復に対する期待は低下しつつある。7-9月期の成長率はプラスに転換すると予想されているが、雇用情勢の急速な改善が必要不可欠の条件となることから、景気回復のペースは当初の予想よりも緩慢になるとの見方が増えている。為替については、米長期金利の上昇がある程度抑制される可能性が高まっていることから、「リスク選好的なドル買いが大きく広がるような状況ではない」との見方が増えているようだ。 《CS》',
          site: {
            id: 3,
            name: 'ロイター',
            file_id: 3,
            status: 1,
            description: '',
            updated_at: 1591064201,
            url: 'https://jp.reuters.com'
          },
          site_id: 3,
          status: 3,
          title: '今日の為替市場ポイント：米国株安などを意識してリスク選好のドル買い抑制も - ロイター',
          type: 0,
          update_origin_at: null,
          updated_at: 1591169939,
          views: 695,
          youtube_id: '',
          youtube_url: null,
          youtube_views: 740
        },
        {
          admin_note: null,
          article_category: {
            created_at: 1591064201,
            description: '',
            id: 12,
            name: 'ガバナンス',
            parent: null,
            parent_id: 0,
            position: 11,
            updated_at: 1591064201
          },
          article_category_id: 12,
          create_origin_at: null,
          created_at: 1589538088,
          description: null,
          hash_md5: null,
          id: 1932,
          image_path: 'https://s4.reutersmedia.net/resources_v2/images/rcom-default.png',
          images: [],
          last_crawl_at: null,
          origin_url: 'https://jp.reuters.com/article/idJP00093400_20200513_01720200512',
          origin_url_md5: null,
          public_end_at: 1689338418,
          public_start_at: 1589512888,
          related_article: null,
          short_description: '*08:37JST 今日の為替市場ポイント:◆ユーロ編◆リスク選好的なユーロ買い・円売りがただちに拡大する可能性は低い見通し ユーロ・ドルは、1.0341ドル（2017/01/03）まで下落したが、1.2537ドル（2018/01/25）まで上昇。ユーロ・円は、英国民投票で欧州連合（EU）からの離脱が決定し、一時109円57銭（2016/06/24）まで急落。その後114円85銭（2017/04/17）から137円50銭（2018/2/2）まで買われた。ユーロ圏経済の先行き不安や、ウイルス感染の現状などがユーロの反発を抑えているようだ。新たなユーロ買い材料が提供されない場合、リスク選好的なユーロ買い・円売りがただちに拡大する可能性は低いとみられる。 【ユーロ売り要因】 ・欧州諸国におけるウイルス感染者の増加 ・ウイルス感染の拡大を巡って米中対立の懸念 ・ドイツ憲法裁は欧州中央銀行（ECB）の一部行動を違憲と指摘 【ユーロ買い要因】 ・ドイツ、フランスなどの財政出動 ・ドイツ、スペイン、イタリアの段階的な経済再開計画 ・ウイルス感染拡大の抑制期待 《CS》',
          site: {
            id: 3,
            name: 'ロイター',
            file_id: 3,
            status: 1,
            description: '',
            updated_at: 1591064201,
            url: 'https://jp.reuters.com'
          },
          site_id: 3,
          status: 3,
          title: '今日の為替市場ポイント:◆ユーロ編◆リスク選好的なユーロ買い・円売りがただちに拡大する可能性は低い見通し - ロイター',
          type: 0,
          update_origin_at: null,
          updated_at: 1591169939,
          views: 141,
          youtube_id: '',
          youtube_url: null,
          youtube_views: 400
        }
      ],
      links: {
        first: 'http://content-management-api.test/api/admin/article?page=1',
        last: 'http://content-management-api.test/api/admin/article?page=195',
        prev: null,
        next: 'http://content-management-api.test/api/admin/article?page=2'
      },
      meta: {
        current_page: 1,
        from: 1,
        last_page: 195,
        path: 'http://content-management-api.test/api/admin/article',
        per_page: 10,
        to: 10,
        total: 1941
      }
    }
  },

  getArticle: () => {
    return {
      data: {
        admin_note: null,
        article_category: {
          created_at: 1591064201,
          description: '',
          id: 6,
          name: '環境',
          parent: null,
          parent_id: 0,
          position: 5,
          updated_at: 1591064201
        },
        article_category_id: 6,
        create_origin_at: 1589327722,
        created_at: 1589538089,
        description: '<div class="StandardArticleBody_body"><div class="PrimaryAsset_container"><div class="Image_container Image_overlay-caption" tabindex="-1"><figure class="Image_zoom" style="padding-bottom:"><div class="LazyImage_container LazyImage_dark" style="background-image:none"><img src="//s4.reutersmedia.net/resources/r/?m=02&amp;d=20200512&amp;t=2&amp;i=1518409665&amp;r=LYNXMPEG4B24X&amp;w=20" aria-label="　自動車レースＦ１、フェラーリは１２日、個人総合優勝４回のセバスチャン・フェテルとの契約を更新せず、今年限りでチームを離れることを発表した。メルボルンで２０２０年３月撮影（２０２０年　ロイター/Edgar Su）"><div class="LazyImage_image LazyImage_fallback" style="background-image:url(//s4.reutersmedia.net/resources/r/?m=02&amp;d=20200512&amp;t=2&amp;i=1518409665&amp;r=LYNXMPEG4B24X&amp;w=20);background-position:center center;background-color:inherit"></div></div></figure><div class="Image_expand-button" aria-label="Expand Image Slideshow" role="button" tabindex="0"><svg height="18px" width="18px" version="1.1" viewbox="0 0 18 18" focusable="false"><path d="M16.2928932,1 L12.5,1 C12.2238576,1 12,0.776142375 12,0.5 C12,0.223857625 12.2238576,0 12.5,0 L17.5,0 C17.7761424,0 18,0.223857625 18,0.5 L18,5.5 C18,5.77614237 17.7761424,6 17.5,6 C17.2238576,6 17,5.77614237 17,5.5 L17,1.70710678 L12.8535534,5.85355339 C12.6582912,6.04881554 12.3417088,6.04881554 12.1464466,5.85355339 C11.9511845,5.65829124 11.9511845,5.34170876 12.1464466,5.14644661 L16.2928932,1 Z M1,16.2928932 L5.14644661,12.1464466 C5.34170876,11.9511845 5.65829124,11.9511845 5.85355339,12.1464466 C6.04881554,12.3417088 6.04881554,12.6582912 5.85355339,12.8535534 L1.70710678,17 L5.5,17 C5.77614237,17 6,17.2238576 6,17.5 C6,17.7761424 5.77614237,18 5.5,18 L0.5,18 C0.223857625,18 0,17.7761424 0,17.5 L0,12.5 C0,12.2238576 0.223857625,12 0.5,12 C0.776142375,12 1,12.2238576 1,12.5 L1,16.2928932 Z"></path></svg></div><figcaption><div class="Image_caption"><span>　自動車レースＦ１、フェラーリは１２日、個人総合優勝４回のセバスチャン・フェテルとの契約を更新せず、今年限りでチームを離れることを発表した。メルボルンで２０２０年３月撮影（２０２０年　ロイター/Edgar Su）</span></div></figcaption></div></div><p>［ロンドン　１２日　ロイター］ - 自動車レースＦ１、フェラーリは１２日、個人総合優勝４回のセバスチャン・フェテル（３２、ドイツ）との契約を更新せず、今年限りでチームを離れることを発表した。 </p><p>両者は契約延長に関して話し合いを設けていたが、合意には至らなかった。ドイツのメディアは１１日夜、フェテルがチームの提示した条件を受け入れなかったと報じていた。その条件は１年契約で報酬も減額だったという。 </p><p>フェテルは「このスポーツで最高の結果を出すためには、全関係者が完璧なハーモニーを奏でることが不可欠。チームと私は今シーズンが終わった後も、ともに戦いたいという同じ思いをもはや持っていないことを認識した。財政的問題は今回の共同決定に関係していない」と語った。 </p><p>２０１５年にフェラーリに加入したフェテルは、同チームで史上３番目に多い通算１４勝を記録している。</p><div class="StandardArticleBody_copyrightNotice"><p></p><p></p></div><div class="StandardArticleBody_trustBadgeContainer"><span class="StandardArticleBody_trustBadgeTitle">私たちの行動規範：</span><span class="trustBadgeUrl"><a href="http://thomsonreuters.com/en/about-us/trust-principles.html">トムソン・ロイター「信頼の原則」</a></span></div></div>',
        hash_md5: 'e8766e818b17eacd6e3a309b49446448',
        id: 1941,
        image_path: 'https://s4.reutersmedia.net/resources/r/?m=02&d=20200512&t=2&i=1518409665&w=1200&r=LYNXMPEG4B24X',
        images: [
          {
            created_at: 1591064267,
            id: 19,
            path: '/upload/demo2.png',
            pivot: {
              article_id: 1941,
              file_id: 19,
              created_at: null,
              updated_at: null
            },
            updated_at: 1591064267
          }
        ],
        last_crawl_at: 1589512889,
        origin_url: 'https://jp.reuters.com/article/f-idJPKBN22O3GG',
        origin_url_md5: 'a8eab84ac9e120b8e35fec301d3ac978',
        public_end_at: 1689338418,
        public_start_at: 1589512889,
        related_article: null,
        short_description: '自動車レースＦ１、フェラーリは１２日、個人総合優勝４回のセバスチャン・フェテル（３２、ドイツ）との契約を更新せず、今年限りでチームを離れることを発表した。',
        site: {
          id: 3,
          name: 'ロイター',
          file_id: 3,
          status: 1,
          description: '',
          updated_at: 1591064201,
          url: 'https://jp.reuters.com'
        },
        site_id: 3,
        status: 3,
        title: 'Ｆ１＝フェテル、20年限りでフェラーリを離れることに - ロイター',
        type: 0,
        update_origin_at: 1589327722,
        updated_at: 1591169939,
        views: 179,
        youtube_id: '',
        youtube_url: null,
        youtube_views: 691
      }
    }
  },

  createArticle: () => {
    return {
      data: {
        admin_note: null,
        article_category: {
          created_at: 1591064201,
          description: '',
          id: 6,
          name: '環境',
          parent: null,
          parent_id: 0,
          position: 5,
          updated_at: 1591064201
        },
        article_category_id: 6,
        create_origin_at: 1589327722,
        created_at: 1589538089,
        description: '<div class="StandardArticleBody_body"><div class="PrimaryAsset_container"><div class="Image_container Image_overlay-caption" tabindex="-1"><figure class="Image_zoom" style="padding-bottom:"><div class="LazyImage_container LazyImage_dark" style="background-image:none"><img src="//s4.reutersmedia.net/resources/r/?m=02&amp;d=20200512&amp;t=2&amp;i=1518409665&amp;r=LYNXMPEG4B24X&amp;w=20" aria-label="　自動車レースＦ１、フェラーリは１２日、個人総合優勝４回のセバスチャン・フェテルとの契約を更新せず、今年限りでチームを離れることを発表した。メルボルンで２０２０年３月撮影（２０２０年　ロイター/Edgar Su）"><div class="LazyImage_image LazyImage_fallback" style="background-image:url(//s4.reutersmedia.net/resources/r/?m=02&amp;d=20200512&amp;t=2&amp;i=1518409665&amp;r=LYNXMPEG4B24X&amp;w=20);background-position:center center;background-color:inherit"></div></div></figure><div class="Image_expand-button" aria-label="Expand Image Slideshow" role="button" tabindex="0"><svg height="18px" width="18px" version="1.1" viewbox="0 0 18 18" focusable="false"><path d="M16.2928932,1 L12.5,1 C12.2238576,1 12,0.776142375 12,0.5 C12,0.223857625 12.2238576,0 12.5,0 L17.5,0 C17.7761424,0 18,0.223857625 18,0.5 L18,5.5 C18,5.77614237 17.7761424,6 17.5,6 C17.2238576,6 17,5.77614237 17,5.5 L17,1.70710678 L12.8535534,5.85355339 C12.6582912,6.04881554 12.3417088,6.04881554 12.1464466,5.85355339 C11.9511845,5.65829124 11.9511845,5.34170876 12.1464466,5.14644661 L16.2928932,1 Z M1,16.2928932 L5.14644661,12.1464466 C5.34170876,11.9511845 5.65829124,11.9511845 5.85355339,12.1464466 C6.04881554,12.3417088 6.04881554,12.6582912 5.85355339,12.8535534 L1.70710678,17 L5.5,17 C5.77614237,17 6,17.2238576 6,17.5 C6,17.7761424 5.77614237,18 5.5,18 L0.5,18 C0.223857625,18 0,17.7761424 0,17.5 L0,12.5 C0,12.2238576 0.223857625,12 0.5,12 C0.776142375,12 1,12.2238576 1,12.5 L1,16.2928932 Z"></path></svg></div><figcaption><div class="Image_caption"><span>　自動車レースＦ１、フェラーリは１２日、個人総合優勝４回のセバスチャン・フェテルとの契約を更新せず、今年限りでチームを離れることを発表した。メルボルンで２０２０年３月撮影（２０２０年　ロイター/Edgar Su）</span></div></figcaption></div></div><p>［ロンドン　１２日　ロイター］ - 自動車レースＦ１、フェラーリは１２日、個人総合優勝４回のセバスチャン・フェテル（３２、ドイツ）との契約を更新せず、今年限りでチームを離れることを発表した。 </p><p>両者は契約延長に関して話し合いを設けていたが、合意には至らなかった。ドイツのメディアは１１日夜、フェテルがチームの提示した条件を受け入れなかったと報じていた。その条件は１年契約で報酬も減額だったという。 </p><p>フェテルは「このスポーツで最高の結果を出すためには、全関係者が完璧なハーモニーを奏でることが不可欠。チームと私は今シーズンが終わった後も、ともに戦いたいという同じ思いをもはや持っていないことを認識した。財政的問題は今回の共同決定に関係していない」と語った。 </p><p>２０１５年にフェラーリに加入したフェテルは、同チームで史上３番目に多い通算１４勝を記録している。</p><div class="StandardArticleBody_copyrightNotice"><p></p><p></p></div><div class="StandardArticleBody_trustBadgeContainer"><span class="StandardArticleBody_trustBadgeTitle">私たちの行動規範：</span><span class="trustBadgeUrl"><a href="http://thomsonreuters.com/en/about-us/trust-principles.html">トムソン・ロイター「信頼の原則」</a></span></div></div>',
        hash_md5: 'e8766e818b17eacd6e3a309b49446448',
        id: 1941,
        image_path: 'https://s4.reutersmedia.net/resources/r/?m=02&d=20200512&t=2&i=1518409665&w=1200&r=LYNXMPEG4B24X',
        images: [
          {
            created_at: 1591064267,
            id: 19,
            path: '/upload/demo2.png',
            pivot: {
              article_id: 1941,
              file_id: 19,
              created_at: null,
              updated_at: null
            },
            updated_at: 1591064267
          }
        ],
        last_crawl_at: 1589512889,
        origin_url: 'https://jp.reuters.com/article/f-idJPKBN22O3GG',
        origin_url_md5: 'a8eab84ac9e120b8e35fec301d3ac978',
        public_end_at: 1689338418,
        public_start_at: 1589512889,
        related_article: null,
        short_description: '自動車レースＦ１、フェラーリは１２日、個人総合優勝４回のセバスチャン・フェテル（３２、ドイツ）との契約を更新せず、今年限りでチームを離れることを発表した。',
        site: {
          id: 3,
          name: 'ロイター',
          file_id: 3,
          status: 1,
          description: '',
          updated_at: 1591064201,
          url: 'https://jp.reuters.com'
        },
        site_id: 3,
        status: 3,
        title: 'Ｆ１＝フェテル、20年限りでフェラーリを離れることに - ロイター',
        type: 0,
        update_origin_at: 1589327722,
        updated_at: 1591169939,
        views: 179,
        youtube_id: '',
        youtube_url: null,
        youtube_views: 691
      }
    }
  },

  updateArticle: () => {
    return {
      data: {
        admin_note: null,
        article_category: {
          created_at: 1591064201,
          description: '',
          id: 6,
          name: '環境',
          parent: null,
          parent_id: 0,
          position: 5,
          updated_at: 1591064201
        },
        article_category_id: 6,
        create_origin_at: 1589327722,
        created_at: 1589538089,
        description: '<div class="StandardArticleBody_body"><div class="PrimaryAsset_container"><div class="Image_container Image_overlay-caption" tabindex="-1"><figure class="Image_zoom" style="padding-bottom:"><div class="LazyImage_container LazyImage_dark" style="background-image:none"><img src="//s4.reutersmedia.net/resources/r/?m=02&amp;d=20200512&amp;t=2&amp;i=1518409665&amp;r=LYNXMPEG4B24X&amp;w=20" aria-label="　自動車レースＦ１、フェラーリは１２日、個人総合優勝４回のセバスチャン・フェテルとの契約を更新せず、今年限りでチームを離れることを発表した。メルボルンで２０２０年３月撮影（２０２０年　ロイター/Edgar Su）"><div class="LazyImage_image LazyImage_fallback" style="background-image:url(//s4.reutersmedia.net/resources/r/?m=02&amp;d=20200512&amp;t=2&amp;i=1518409665&amp;r=LYNXMPEG4B24X&amp;w=20);background-position:center center;background-color:inherit"></div></div></figure><div class="Image_expand-button" aria-label="Expand Image Slideshow" role="button" tabindex="0"><svg height="18px" width="18px" version="1.1" viewbox="0 0 18 18" focusable="false"><path d="M16.2928932,1 L12.5,1 C12.2238576,1 12,0.776142375 12,0.5 C12,0.223857625 12.2238576,0 12.5,0 L17.5,0 C17.7761424,0 18,0.223857625 18,0.5 L18,5.5 C18,5.77614237 17.7761424,6 17.5,6 C17.2238576,6 17,5.77614237 17,5.5 L17,1.70710678 L12.8535534,5.85355339 C12.6582912,6.04881554 12.3417088,6.04881554 12.1464466,5.85355339 C11.9511845,5.65829124 11.9511845,5.34170876 12.1464466,5.14644661 L16.2928932,1 Z M1,16.2928932 L5.14644661,12.1464466 C5.34170876,11.9511845 5.65829124,11.9511845 5.85355339,12.1464466 C6.04881554,12.3417088 6.04881554,12.6582912 5.85355339,12.8535534 L1.70710678,17 L5.5,17 C5.77614237,17 6,17.2238576 6,17.5 C6,17.7761424 5.77614237,18 5.5,18 L0.5,18 C0.223857625,18 0,17.7761424 0,17.5 L0,12.5 C0,12.2238576 0.223857625,12 0.5,12 C0.776142375,12 1,12.2238576 1,12.5 L1,16.2928932 Z"></path></svg></div><figcaption><div class="Image_caption"><span>　自動車レースＦ１、フェラーリは１２日、個人総合優勝４回のセバスチャン・フェテルとの契約を更新せず、今年限りでチームを離れることを発表した。メルボルンで２０２０年３月撮影（２０２０年　ロイター/Edgar Su）</span></div></figcaption></div></div><p>［ロンドン　１２日　ロイター］ - 自動車レースＦ１、フェラーリは１２日、個人総合優勝４回のセバスチャン・フェテル（３２、ドイツ）との契約を更新せず、今年限りでチームを離れることを発表した。 </p><p>両者は契約延長に関して話し合いを設けていたが、合意には至らなかった。ドイツのメディアは１１日夜、フェテルがチームの提示した条件を受け入れなかったと報じていた。その条件は１年契約で報酬も減額だったという。 </p><p>フェテルは「このスポーツで最高の結果を出すためには、全関係者が完璧なハーモニーを奏でることが不可欠。チームと私は今シーズンが終わった後も、ともに戦いたいという同じ思いをもはや持っていないことを認識した。財政的問題は今回の共同決定に関係していない」と語った。 </p><p>２０１５年にフェラーリに加入したフェテルは、同チームで史上３番目に多い通算１４勝を記録している。</p><div class="StandardArticleBody_copyrightNotice"><p></p><p></p></div><div class="StandardArticleBody_trustBadgeContainer"><span class="StandardArticleBody_trustBadgeTitle">私たちの行動規範：</span><span class="trustBadgeUrl"><a href="http://thomsonreuters.com/en/about-us/trust-principles.html">トムソン・ロイター「信頼の原則」</a></span></div></div>',
        hash_md5: 'e8766e818b17eacd6e3a309b49446448',
        id: 1941,
        image_path: 'https://s4.reutersmedia.net/resources/r/?m=02&d=20200512&t=2&i=1518409665&w=1200&r=LYNXMPEG4B24X',
        images: [
          {
            created_at: 1591064267,
            id: 19,
            path: '/upload/demo2.png',
            pivot: {
              article_id: 1941,
              file_id: 19,
              created_at: null,
              updated_at: null
            },
            updated_at: 1591064267
          }
        ],
        last_crawl_at: 1589512889,
        origin_url: 'https://jp.reuters.com/article/f-idJPKBN22O3GG',
        origin_url_md5: 'a8eab84ac9e120b8e35fec301d3ac978',
        public_end_at: 1689338418,
        public_start_at: 1589512889,
        related_article: null,
        short_description: '自動車レースＦ１、フェラーリは１２日、個人総合優勝４回のセバスチャン・フェテル（３２、ドイツ）との契約を更新せず、今年限りでチームを離れることを発表した。',
        site: {
          id: 3,
          name: 'ロイター',
          file_id: 3,
          status: 1,
          description: '',
          updated_at: 1591064201,
          url: 'https://jp.reuters.com'
        },
        site_id: 3,
        status: 3,
        title: 'Ｆ１＝フェテル、20年限りでフェラーリを離れることに - ロイター',
        type: 0,
        update_origin_at: 1589327722,
        updated_at: 1591169939,
        views: 179,
        youtube_id: '',
        youtube_url: null,
        youtube_views: 691
      }
    }
  },

  deleteArticle: () => {
    return {}
  }
}
