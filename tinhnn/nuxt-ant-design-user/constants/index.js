/**
 * User status list
 */
export const USER_STATUS_LIST = [
  { id: 0, name: 'Inactive' },
  { id: 1, name: 'Active' }
]
