module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    '@nuxtjs',
    'prettier',
    'prettier/vue',
    'plugin:prettier/recommended',
    'plugin:nuxt/recommended'
  ],
  plugins: ['prettier'],
  // add your custom rules here
  rules: {
    'no-console': 'off',
    'prettier/prettier': [
      'error',
      {
        printWidth: 80,
        useTabs: false,
        tabWidth: 2,
        semi: false,
        singleQuote: true,
        trailingComma: 'none',
        bracketSpacing: true,
        arrowParens: 'avoid'
      }
    ],
    'vue/html-self-closing': [
      'error',
      {
        html: {
          void: 'always',
          normal: 'always',
          component: 'always'
        },
        svg: 'always',
        math: 'always'
      }
    ],
    'vue/order-in-components': [
      'error',
      {
        order: [
          'layout',
          'components',
          'mixins',
          'middleware',
          'asyncData',
          'fetch',
          'serverPrefetch',
          'data',
          'computed',
          'watch',
          'created',
          'mounted',
          'updated',
          'destroyed',
          'methods'
        ]
      }
    ]
  }
}
